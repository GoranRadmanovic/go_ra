const HtmlWebpackPlugin = require("html-webpack-plugin"),
			MiniCssExtractPlugin = require("mini-css-extract-plugin"),
			FaviconsWebpackPlugin = require('favicons-webpack-plugin');

module.exports = {
	module: {
		rules: [
			{
				test: /\.html$/,
				use: [
					{
						loader: "html-loader",
						options: {
							minimize: true,
							interpolate: true
						 }
					}
				]
			},

			{
				test: /\.js$/,
				exclude: /node-modules/,
				use: {
					loader: "babel-loader"
				}
			},

			{
				test: /\.(png|svg|jpg|jpeg|gif)$/,
				use: [
					"file-loader"
				]
			},

			{
				test: /\.(sass|scss)$/,
				use: [
					"style-loader",
					"css-loader",
					"sass-loader"
				]
			}
		]
	},

	plugins: [
		new HtmlWebpackPlugin({
			template: "./src/index.html",
			filename: "./index.html"
		}),

		new MiniCssExtractPlugin({
			//Options similar to the same options in webpackOptions.output
			//both options are optional
			filename: "[name].css",
			chunkFilename: "[id].css"
		}),

		new FaviconsWebpackPlugin({
			logo: './src/assets/images/favicon/favicon-32x32.png', // svg works too!
			path: "/",                                // Path for overriding default icons path. `string`
			appName: 'FE Test Project',               // Your application's name. `string`
			appShortName: 'FE',                       // Your application's short_name. `string`. Optional. If not set, appName will be used
			appDescription: 'Front end example project', // Your application's description. `string`
			developerName: 'Goran Radmanovic',        // Your (or your developer's) name. `string`
			developerURL: 'https://goranradmanovic.github.io/', // Your (or your developer's) URL. `string`
			dir: "auto",                              // Primary text direction for name, short_name, and description
			lang: "en-US",                            // Primary language for name and short_name
			background: "#fff",                       // Background colour for flattened icons. `string`
			theme_color: "#fff",                      // Theme color user for example in Android's task switcher. `string`
			appleStatusBarStyle: "black-translucent", // Style for Apple status bar: "black-translucent", "default", "black". `string`
			display: "standalone",                    // Preferred display mode: "fullscreen", "standalone", "minimal-ui" or "browser". `string`
			orientation: "any",                       // Default orientation: "any", "natural", "portrait" or "landscape". `string`
			scope: "/",                               // set of URLs that the browser considers within your app
			icons: {
					android: true,              // Create Android homescreen icon. `boolean` or `{ offset, background, mask, overlayGlow, overlayShadow }`
					appleIcon: true,            // Create Apple touch icons. `boolean` or `{ offset, background, mask, overlayGlow, overlayShadow }`
					appleStartup: true,         // Create Apple startup images. `boolean` or `{ offset, background, mask, overlayGlow, overlayShadow }`
					coast: true,                // Create Opera Coast icon. `boolean` or `{ offset, background, mask, overlayGlow, overlayShadow }`
					favicons: true,             // Create regular favicons. `boolean` or `{ offset, background, mask, overlayGlow, overlayShadow }`
			}
		})
	]
}
