import Glide from '@glidejs/glide';
import Header from './classes/Header.js';
import Navigation from './classes/Navigation.js';
import Content from './classes/Content.js';
import InfoForm from './classes/InfoForm.js';
import DataBindings from './classes/DataBindings.js';
import UserInfo from './classes/UserInfo.js';
import InfoPopup from './classes/InfoPopup.js';

export default function init() {

  //Navigation slider
  new Glide('.glide', {
    type: 'slider',
    startAt: 0,
    perView: 4
  }).mount();

  //Header
  let header = new Header();
  header.openHeaderLogoutMenu();
  header.closeHeaderLogoutMenu();
  header.openHeaderAddFolowers();

  //Navigation
  let navigation = new Navigation();
  navigation.openTabContent();

  //Content
  let content = new Content();
  content.aboutEditBtn();
  content.aboutCancelBtn();
  content.openPopup();
  content.closePopup();

  //Output user initial (default) data
  let userInfo = new UserInfo();
  userInfo.outputUserData();

  let infoForm = new InfoForm(userInfo.userData);
  infoForm.saveFormData();

  let infoPopup = new InfoPopup(userInfo.userData);
  infoPopup.savePopupFieldValue();

  let dataBindings = new DataBindings(userInfo.userData);
  dataBindings.dataChangeWatcher();
}
