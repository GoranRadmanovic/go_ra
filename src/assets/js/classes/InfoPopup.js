class InfoPopup {

  constructor(data) {
    this.userData = data;
    this.popupInputFields = this.getPageElements('#title-popup, #web-popup, #phone-popup, #location-popup');
    this.popupSaveButtons = this.getPageElements('.content__page__body__popup__actions__cta.save');
    this.userInfoElements = this.getPageElements('#headerInfoTitle, #headerInfoLocation, #headerInfoPhone, #aboutTitle, #aboutWeb, #aboutPhone, #aboutLocation');
  }

  //Get single el.
  getPageElement(element) {
      return document.querySelector(element);
  }

  //Get all el.
  getPageElements(element) {
      return document.querySelectorAll(element);
  }

  //Save data from popup fields
  savePopupFieldValue() {
    let fieldName = null;

    this.popupSaveButtons.forEach(button => {

      button.addEventListener('click', event => {
        if (event.target.dataset.inputid !== 'title-popup') {

          let inputField = this.getPageElement(`#${event.target.dataset.inputid}`);

          //Check if field is not empty
          if (inputField.value !== '') {
            fieldName = inputField.name;
            this.userData[inputField.name] = inputField.value;
          }

        } else {
          let inputFieldTitle = this.getPageElement(`#${event.target.dataset.inputid}`);

          //Check if field is not empty
          if (inputFieldTitle.value !== '') {
            fieldName = inputFieldTitle.name;
            let username = inputFieldTitle.value.split(' ');

            this.userData.firstName = username[0] ? username[0] : '';
            this.userData.lastName = username[1] ? username[1] : '';
          }
        }

        this.updateUserInfo(this.userData, fieldName);
        this.getPageElement(`#popup-${event.target.dataset.popupid}`).classList.remove('show');
      });
    });
  }

  updateUserInfo(data, field) {
    if (field !== 'title') {
      this.userInfoElements.forEach(element => {

        if (element.dataset.fieldname == field) {
          element.textContent = data[field];
        }
      });
    } else {
      let username = data.firstName + ' ' + data.lastName;
      this.userInfoElements[0].textContent = username;
      this.userInfoElements[3].textContent = username;
    }
  }
}

export default InfoPopup;
