class Content {

  constructor() {
    this.aboutEditButton = this.getPageElement('.content__page__header__btn');
    this.aboutForm = this.getPageElement('#aboutForm');
    this.aboutInfo = this.getPageElement('#aboutInfo');
    this.aboutCancelButton = this.getPageElement('#cancel-btn');
    this.aboutSaveButton = this.getPageElement('#save-btn');

    //Popup buttons
    this.popupButtons = this.getPageElements('.content__page__body__title__btn, .content__page__body__web__btn, .content__page__body__phone__btn, .content__page__body__location__btn');
    this.popups = this.getPageElements('#popup-1, #popup-2, #popup-3, #popup-4');
    this.popupCancelButtons = this.getPageElements('.content__page__body__popup__actions__cta.cancel');
  }

  //Get single el.
  getPageElement(element) {
      return document.querySelector(element);
  }

  //Get all el.
  getPageElements(element) {
      return document.querySelectorAll(element);
  }

  aboutEditBtn() {
    this.aboutEditButton.addEventListener('click', () => {
      this.aboutInfo.style.display = 'none';
      this.aboutEditButton.style.display = 'none';
      this.aboutForm.style.display = 'block';
      this.aboutCancelButton.style.display = 'block';
      this.aboutSaveButton.style.display = 'block';
    });
  }

  aboutCancelBtn() {
    this.aboutCancelButton.addEventListener('click', () => {
      this.aboutForm.style.display = 'none';
      this.aboutCancelButton.style.display = 'none';
      this.aboutSaveButton.style.display = 'none';
      this.aboutInfo.style.display = 'flex';
      this.aboutEditButton.style.display = 'block';
    });
  }

  openPopup() {
    this.popupButtons.forEach((button, index) => {

      button.addEventListener('click', event => {

        this.popups.forEach(popup => {
            popup.classList.remove('show');
        });

        this.getPageElement(`#popup-${index + 1}`).classList.add('show');
      });
    });
  }

  closePopup() {
    this.popupCancelButtons.forEach(button => {
      button.addEventListener('click', event => {
        this.getPageElement(`#popup-${event.target.dataset.popupid}`).classList.remove('show');
      });
    });
  }
}

export default Content;
