class Navigation {

  constructor() {
    this.tabs = this.getPageElements('.header__nav__track__list__item'),
    this.desktopTabs = this.getPageElements('.header__nav__track__list__item--desktop'),
    this.activeTab = null,
    this.activeTabPage = null,
    this.currentActiveTabPage = null;
  }

  //Get single el.
  getPageElement(element) {
      return document.querySelector(element);
  }

  //Get all el.
  getPageElements(element) {
      return document.querySelectorAll(element);
  }

  openTabContent() {
    this.tabContent(this.tabs);
    this.tabContent(this.desktopTabs, true);
  }

  tabContent(elements, desktop = null) {
    elements.forEach(element => {
      //Click on nav tab
      element.addEventListener('click', event => {
        //Default active tab
        this.activeTab = (desktop) ? this.getPageElement('.header__nav__track__list__item--desktop.active') : this.getPageElement('.header__nav__track__list__item.active');

        //For mobile nav
        if (!desktop) {
          this.activeTab.style.borderColor = 'transparent';
        }

        this.activeTab.classList.remove('active');

        //Current active tab
        event.target.classList.add('active');

        //Default Active page
        this.activeTabPage = this.getPageElement('.content__page.active');
        this.activeTabPage.classList.remove('active');

        //Current Active page
        this.currentActiveTabPage = this.getPageElement(`#${event.target.dataset.linkName}`);
        this.currentActiveTabPage.classList.add('active');
      });
    });
  }
}

export default Navigation;
