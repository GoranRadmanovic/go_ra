class Header {
  constructor() {
    this.headerLogoutBtn = this.getPageElement('.header__card__btn__cta'),
    this.headerCardMenu = this.getPageElement('.header__card__btn__menu');
    this.followers = this.getPageElement('#desktopNavFollowers');
    this.followersPopup = this.getPageElement('.header__card__followers__popup');
  }

  //Get single el.
  getPageElement(element) {
      return document.querySelector(element);
  }

  //Get all el.
  getPageElements(element) {
      return document.querySelectorAll(element);
  }

  openHeaderLogoutMenu() {
    this.headerLogoutBtn.addEventListener('click', event => {
        this.headerCardMenu.classList.toggle('menu-open');
    });
  }

  closeHeaderLogoutMenu() {
    document.addEventListener('mouseup', event => {
      if (event.target !== this.headerLogoutBtn && event.target.localName !== 'li') {
          this.headerCardMenu.classList.remove('menu-open');
      }
    });
  }

  openHeaderAddFolowers() {
    this.followers.addEventListener('mouseover', event => {
        this.followersPopup.classList.add('popup-open');
    });

    this.followers.addEventListener('mouseout', event => {
        this.followersPopup.classList.remove('popup-open');
    });
  }
}

export default Header;
