class DataBindings {
  constructor(data) {
    this.state = data;
    this.inputFileds = this.getPageElements('#firstName, #lastName, #website, #phone, #location');
    this.outputInfoElements = this.getPageElements('#headerInfoTitle, #headerInfoPhone, #headerInfoLocation');

    //Popup input fields
    this.popupInputFileds = this.getPageElements('#title-popup, #web-popup, #phone-popup, #location-popup');
    this.outputUserPopupInfo = this.getPageElements('#headerInfoTitle, #headerInfoLocation, #headerInfoPhone, #aboutTitle, #aboutWeb, #aboutPhone, #aboutLocation');
  }

  //Get all el.
  getPageElements(element) {
      return document.querySelectorAll(element);
  }

  //Watch for data change
  dataChangeWatcher() {
    this.addInputFiledsListeners(this.inputFileds, this.state);
    this.addInputFiledsListeners(this.popupInputFileds, this.state, true);
  }

  //Add event listener on mobile and popuop input fields
  addInputFiledsListeners(elements, data, singleProp = null) {
    elements.forEach(element => {
      let name = element.name;

      element.addEventListener('keyup', event => {
        this.state[name] = element.value;

        if (name !== 'title') {
          this.state[name] = element.value;
        } else {
          //If user is not provided first or last name
          let username = element.value.split(' ');
          this.state.firstName = username[0] ? username[0] : '';
          this.state.lastName = username[1] ? username[1] : '';
        }

        this.render(name, singleProp);
      });
    });
  }

  //Render data from mobile and popup input fields and output it in header and about section
  render(propertyName, singleChange) {
    let username = this.state.firstName + ' ' + this.state.lastName; //Username

    //Oput data in page header
    this.outputInfoElements.forEach(element => {
      element.textContent = (element.dataset.fieldname !== 'title') ? this.state[element.dataset.fieldname] : username;
    });

    //Render data fro popup input field change
    if (singleChange) {
      //Render only changed data and fields
      switch (propertyName) {
        case 'title':
          this.outputUserPopupInfo[0].textContent = username; //Header info title
          this.outputUserPopupInfo[3].textContent = username; //About info title
          break;

        case 'phone':
          this.outputUserPopupInfo[1].textContent = this.state.phone; //Header info phone
          this.outputUserPopupInfo[5].textContent = this.state.phone; //About info phone
          break;

        case 'location':
          this.outputUserPopupInfo[2].textContent = this.state.location; //Header info location
          this.outputUserPopupInfo[6].textContent = this.state.location; //About info location
          break;

        case 'website':
          this.outputUserPopupInfo[4].textContent = this.state.website; //About info website
          break;

        default:
      }
    }
  }
}

export default DataBindings;
