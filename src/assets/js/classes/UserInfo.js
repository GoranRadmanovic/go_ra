class UserInfo {
  constructor() {
    //Default user data
    this.userData = {
      firstName: 'Jessica',
      lastName: 'Parker',
      website: 'www.seller.com',
      phone: '(949) 325 - 68594',
      location: 'Newport Beach CA'
    };

    this.aboutInfoElements = this.getPageElements('.header__card__info__leftbox__title, .header__card__info__leftbox__phone__text, .header__card__info__leftbox__location__text, .content__page__body__title__text, .content__page__body__web__text, .content__page__body__phone__text, .content__page__body__location__text');
    this.inputFields = this.getPageElements('#title-popup, #web-popup, #phone-popup, #location-popup, #firstName, #lastName, #website, #phone, #location');
  }

  //Get all el.
  getPageElements(element) {
    return document.querySelectorAll(element);
  }

  //Output default user data
  outputUserData() {
    let username = this.userData.firstName + ' ' + this.userData.lastName;

    this.setDefaultUserData(this.aboutInfoElements, this.userData, username);
    this.setDefaultUserData(this.inputFields, this.userData, username);
  }

  setDefaultUserData(elements, data, userName) {

    elements.forEach(element => {
      let fieldName = (element.hasAttribute("data-fieldname")) ? element.dataset.fieldname : element.name;

      //Mobile form inputs and popup inputs
      if (element.hasAttribute("value")) {
        if (fieldName !== 'title') {
          element.value = data[fieldName];
        } else {
          element.value = userName;
        }
      } else {
        //Header and about info
        if (fieldName !== 'title') {
          element.textContent = data[fieldName];
        } else {
          element.textContent = userName;
        }
      }
    });
  }
}

export default UserInfo;
