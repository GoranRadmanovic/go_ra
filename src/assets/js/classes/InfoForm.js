class InfoForm {
  constructor(data) {
    this.data = data;
    this.saveButton = this.getPageElement('#save-btn');
    this.profileInfoForm = this.getPageElement('#profileInfoForm');
    this.aboutInfoElements = this.getPageElements('.content__page__body__title__text, .content__page__body__web__text, .content__page__body__phone__text, .content__page__body__location__text');
    this.contentInfoElements = this.getPageElements('#aboutForm, #cancel-btn, #save-btn, #aboutInfo, .content__page__header__btn');
    this.formData = null;
  }

  //Get single el.
  getPageElement(element) {
      return document.querySelector(element);
  }

  //Get all el.
  getPageElements(element) {
      return document.querySelectorAll(element);
  }

  //Save data from mobile forme
  saveFormData() {
    this.profileInfoForm.addEventListener('submit', event => {
      event.preventDefault();

      this.formData = new FormData(this.profileInfoForm);

      this.data.firstName = this.formData.get('firstName');
      this.data.lastName = this.formData.get('lastName');
      this.data.website = this.formData.get('website');
      this.data.phone = this.formData.get('phone');
      this.data.location = this.formData.get('location');

      this.outputSavedData(this.data);
      this.closeInfoForm();
    });
  }

  outputSavedData(data) {
    //Username
    let username = data.firstName + ' ' + data.lastName;

    this.aboutInfoElements.forEach(element => {
      element.textContent = (element.dataset.fieldname == 'title') ? username : data[element.dataset.fieldname];
    });
  }

  closeInfoForm() {
    this.contentInfoElements[4].style.display = 'none'; //About info form
    this.contentInfoElements[1].style.display = 'none'; //About info form cancel btn
    this.contentInfoElements[2].style.display = 'none'; //About info form save btn
    this.contentInfoElements[3].style.display = 'flex'; // About info view
    this.contentInfoElements[0].style.display = 'block'; //Edit info btn
  }
}

export default InfoForm;
