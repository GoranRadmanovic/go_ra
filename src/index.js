//Importing SASS files
import './assets/sass/main.sass';

//Import init JS
import init from './assets/js/init.js';

//Wait for document is loaded
document.addEventListener('DOMContentLoaded', _ => {
  init();
});
